def reverser(&prc)
  words = prc.call.split
  words.each(&:reverse!).join(" ")
end

def adder(add = 1, &prc)
  prc.call + add
end

def repeater(num_times = 1, &prc)
  num_times.times { prc.call }
end
