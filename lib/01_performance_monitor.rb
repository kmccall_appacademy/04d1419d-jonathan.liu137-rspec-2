def measure(num_times = 1, &prc)
  start_time = Time.now
  num_times.times { prc.call }
  total_time = Time.now - start_time
  average_time = total_time / num_times

  return average_time if num_times > 1
  total_time
end
